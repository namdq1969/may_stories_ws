package com.namqdam.maystories.entity

/**
 * Created by namdam on 02/20/2017.
 */

data class Country(val name: String, val code: String)

data class Island(val name: String, val country: Country)